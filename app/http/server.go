package http

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/backend/gost-doc/app/config"
	"gitlab.com/backend/gost-doc/app/http/handlers"
	jwt_models "gitlab.com/backend/gost-doc/app/models/jwt"
	"gitlab.com/backend/gost-doc/app/repository"
	"gitlab.com/f4lc09/logger"
)

const defaultPort = "8080"

type gostDocServer struct {
	cfg    config.Config
	db     repository.Driver
	router *gin.Engine
}

func NewGostDocServer(cfg config.Config, db repository.Driver) *gostDocServer {
	return &gostDocServer{
		cfg: cfg,
		db:  db,
	}
}

func (s *gostDocServer) Serve() {
	port := s.cfg.StringParamByName("port")
	if port == "" {
		port = defaultPort
	}

	s.router = gin.New()
	s.router.Use(
		gin.Recovery(),
		handlers.LogRequest(),
		handlers.HeadersMiddleware(),
	)

	jwtService := jwt_models.NewJWTService(s.cfg)
	routerGroup := s.router.Group("api")
	handlers.NewCommonHandler(routerGroup)
	handlers.NewAuthHandler(s.db, routerGroup, jwtService)
	handlers.NewDocHandler(s.db, routerGroup, jwtService)

	logger.Logf("Server started on port %s", port)
	err := http.ListenAndServe(":"+port, s.router)
	if err != nil {
		logger.Error(err)
		return
	}
}
