package models

import "github.com/golang-jwt/jwt/v5"

type RegistrationRequest struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginRequest struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type AuthToken struct {
	jwt.RegisteredClaims
	UserId int64 `json:"id"`
}
