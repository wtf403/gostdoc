package models

type CreateDocRequest struct {
	Title  string  `json:"title"`
	Status *string `json:"status,omitempty"`
}

type Document struct {
	DocRef string    `json:"doc_ref"`
	Title  string    `json:"title"`
	Role   string    `json:"role_name"`
	Users  []DocUser `json:"users"`
}

type DocUser struct {
	Id       int64  `json:"id"`
	Username string `json:"username"`
}

type ChangePrivacyRequest struct {
	DocId  int64  `json:"doc_id"`
	Status string `json:"status"`
}
