package config_models

type Main struct {
	Database       `yaml:"repository"`
	Server         `yaml:"httpServer"`
	Authentication `yaml:"auth"`
}

type Database struct {
	DSN string `yaml:"dsn"`
}

type Server struct {
	Port string `yaml:"port"`
}

type Authentication struct {
	SigningKey string `yaml:"signingKey"`
	TokenTTL   int    `yaml:"tokenTTL"`
	RefreshTTL int    `yaml:"refreshTTL"`
}
