package errcore

type ErrorResponse struct {
	Message string `json:"message"`
}

func (e *ErrorResponse) Error() string {
	return e.Message
}

var (
	InvalidJsonError = &ErrorResponse{
		Message: "некорректный запрос",
	}
	UnauthorizedError = &ErrorResponse{
		Message: "отказ в доступе",
	}
	InternalServerError = &ErrorResponse{
		Message: "возникла какая-то ошибка",
	}
)

type UnsignedResponse struct {
	Message interface{} `json:"message"`
}
