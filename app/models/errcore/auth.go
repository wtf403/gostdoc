package errcore

var (
	InvalidCredentialsError = &ErrorResponse{
		Message: "Invalid credentials.",
	}
	EmptyCredentialsError = &ErrorResponse{
		Message: "Empty credentials.",
	}
	InvalidAuthorizationHeaderError = &ErrorResponse{
		Message: "Invalid authorization header.",
	}
	FailedToSignTokenError = &ErrorResponse{
		Message: "Failed to sign token.",
	}
	UnableToParseTokenError = &ErrorResponse{
		Message: "Unable to parse token.",
	}
	UsernameAlreadyTakenError = &ErrorResponse{
		Message: "Username is already taken.",
	}
	EmailAlreadyTakenError = &ErrorResponse{
		Message: "Email is already taken.",
	}
	WrongPasswordError = &ErrorResponse{
		Message: "Wrong password.",
	}
)
