package errcore

type DBError struct {
	Message  string
	Internal bool
}
