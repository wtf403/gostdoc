package yaml_config

import (
	"fmt"
	"os"
	"reflect"

	"gitlab.com/f4lc09/logger"
	"gopkg.in/yaml.v3"
)

type config struct {
	configData any
}

func NewYamlConfig(model any) *config {
	return &config{
		configData: model,
	}
}

func (c *config) ReadFromFile(configPath string) {
	rawBytes, err := os.ReadFile(configPath)
	if err != nil {
		logger.Fatalf("Failed to read config: %v", err)
	}

	if err := yaml.Unmarshal(rawBytes, c.configData); err != nil {
		logger.Fatalf("Failed to unmarshal config: %v", err)
	}
}

func (c *config) StringParamByName(paramName string) string {
	param := c.paramByLastName(paramName)
	result, ok := param.(string)
	if !ok {
		return ""
	}
	return result
}

func (c *config) IntParamByName(paramName string) int {
	param := c.paramByLastName(paramName)
	result, ok := param.(int)
	if !ok {
		return 0
	}
	return result
}
func (c *config) BoolParamByName(paramName string) bool {
	param := c.paramByLastName(paramName)
	result, ok := param.(bool)
	if !ok {
		return false
	}
	return result
}

func (c *config) paramByLastName(paramName string) any {
	rvConfig := reflect.ValueOf(c.configData)
	result := c.lookup(rvConfig, paramName)
	if result.IsValid() {
		return result.Interface()
	} else {
		return 0
	}
}

func (c *config) lookup(value reflect.Value, paramName string) reflect.Value {
	emptyValue := reflect.Value{}
	switch value.Kind() {
	case reflect.Struct:
		for idx := 0; idx < value.NumField(); idx++ {
			fmt.Printf("%+v %T\n", value, value)
			fieldTag := value.Type().Field(idx).Tag.Get("yaml")
			field := value.FieldByIndex([]int{idx})
			if fieldTag == paramName {
				return field
			}
			field = c.lookup(field, paramName)
			if field != emptyValue {
				return field
			}
		}
		return emptyValue
	case reflect.Pointer:
		return c.lookup(value.Elem(), paramName)
	}
	return emptyValue
}
