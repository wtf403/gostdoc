package repository

import (
	"gitlab.com/backend/gost-doc/app/config"
	"gitlab.com/backend/gost-doc/app/models/errcore"
)

type Driver interface {
	InitConnection(config.Config)
	ExecSP(string, ...any) ([]byte, *errcore.DBError)
	Close() error
}
