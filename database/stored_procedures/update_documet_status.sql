CREATE OR REPLACE FUNCTION update_document_status(_user_id BIGINT, _js JSON) RETURNS VOID
    LANGUAGE plpgsql AS
$$
DECLARE
    _can_change_doc_type  BOOLEAN;
    _doc_id               BIGINT;
    _status               VARCHAR(7);
BEGIN
    SELECT doc_id,
           status
    INTO _doc_id,
         _status
    FROM JSON_TO_RECORD(_js) AS (doc_id BIGINT,
                                 status VARCHAR(7));

    IF _status NOT IN ('PRIVATE', 'PUBLIC') THEN
        RAISE EXCEPTION 'wrong status, expected private or public';
    end if;

    SELECT true
    INTO _can_change_doc_type
    FROM documents d
        JOIN access_groups ag
            ON ag.doc_id = d.id
        JOIN users u ON u.id=ag.user_id
    WHERE d.id = _doc_id
      AND u.id = _user_id
      AND ag.role_id=1;

    IF _can_change_doc_type THEN
        UPDATE documents
        SET status=_status
        WHERE id=_doc_id;
    ELSE
        RAISE EXCEPTION 'access to the document is denied';
    END IF;
END;
$$;
