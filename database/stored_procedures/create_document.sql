CREATE OR REPLACE FUNCTION create_document(_user_id BIGINT, _js JSON) RETURNS VARCHAR(60)
    LANGUAGE plpgsql AS
$$
DECLARE
    _title               VARCHAR(100);
    _doc_id              BIGINT;
    _status              VARCHAR(7);
    _doc_ref             VARCHAR(60);
    _doc_cfg_id          BIGINT;
    _content_id          BIGINT;
    _owner_role_id       INT;

    _error               VARCHAR(100) = '';
    _dt                  TIMESTAMPTZ  = NOW();
BEGIN
    _dt = NOW() AT TIME ZONE 'Europe/Moscow';

    SELECT title,
           status
    INTO _title,
         _status
    FROM JSON_TO_RECORD(_js) AS s(title    VARCHAR(100),
                                  status   VARCHAR(7));


    IF _title = '' OR _title IS NULL THEN
        _error = 'поле "Заголовок" пустое';
    END IF;


    IF _error != '' THEN
        RAISE EXCEPTION '%' , _error;
    END IF;


    IF NOT EXISTS(SELECT 1
                  FROM users u
                  WHERE u.id = _user_id)
    THEN
        RAISE EXCEPTION 'Пользователь не найден';
    END IF;


    IF _status IS NULL THEN
        _status = 'PRIVATE';
    END IF;


    INSERT INTO document_config(title,
                                subtitle,
                                main_text)
    VALUES ('', '', '')
    RETURNING id
    INTO _doc_cfg_id;


    INSERT INTO document_content(type,
                                 body)
    VALUES ('TITLE',
            '')
    RETURNING id
    INTO _content_id;


    SELECT dr.id
    INTO _owner_role_id
    FROM roles dr
    WHERE dr.name = 'OWNER';

    INSERT INTO documents(title,
                          status,
                          created_at,
                          updated_at,
                          config_id,
                          content_start,
                          doc_ref)
    VALUES (_title,
            _status,
            _dt,
            _dt,
            _doc_cfg_id,
            _content_id,
            CURRVAL('documents_id_seq')::VARCHAR || '_' || gen_random_uuid())
    RETURNING id,
              doc_ref
    INTO _doc_id,
         _doc_ref;

    INSERT INTO access_groups (doc_id,
                               user_id,
                               role_id)
    VALUES (_doc_id,
            _user_id,
            _owner_role_id);

    UPDATE document_content dc
    SET doc_id=_doc_id
    WHERE dc.id = _content_id;

    RETURN _doc_ref;
END;
$$;
