CREATE OR REPLACE FUNCTION user_role_by_doc_ref(_user_id INT, _doc_ref VARCHAR) RETURNS VARCHAR(15)
    LANGUAGE plpgsql AS
$$
DECLARE
    _doc_id            BIGINT;
    _role_id           INT;
    _doc_status        VARCHAR(15);
    _role_name         VARCHAR(15);
    _check_doc_ref     VARCHAR(60);
BEGIN
    SELECT get_doc_id_part(split_part(_doc_ref, '_', 1))
    INTO _doc_id;


    SELECT d.doc_ref
    INTO _check_doc_ref
    FROM documents d
    WHERE d.id=_doc_id;


    IF _doc_ref != _check_doc_ref
    THEN
        RAISE EXCEPTION '%', 'Документ не найден';
    END IF;


    SELECT ag.role_id
    INTO _role_id
    FROM access_groups ag
    WHERE ag.doc_id=_doc_id
    AND ag.user_id=_user_id;


    IF _role_id IS NOT NULL
    THEN
        SELECT name
        INTO _role_name
        FROM roles r
        WHERE r.id=_role_id;

        RETURN _role_name;
    END IF;


    SELECT status
    INTO _doc_status
    FROM documents d
    WHERE d.id=_doc_id;


    SELECT default_user_role
    INTO _role_name
    FROM document_status
    WHERE name=_doc_status;


    IF _role_name IS NULL
    THEN
        RAISE EXCEPTION '%', 'Документ не найден';
    END IF;


    INSERT INTO access_groups (doc_id, user_id, role_id)
    VALUES (_doc_id, _user_id, _role_id);


    RETURN _role_name;
END;
$$;

CREATE OR REPLACE FUNCTION get_doc_id_part(_in VARCHAR(20)) RETURNS BIGINT
  LANGUAGE plpgsql AS
$$
DECLARE
    _out BIGINT;
BEGIN
   SELECT _in::BIGINT
   INTO  _out;
   RETURN _out;
   EXCEPTION WHEN others THEN
       RAISE EXCEPTION 'Некорректная ссылка';
END
$$;
