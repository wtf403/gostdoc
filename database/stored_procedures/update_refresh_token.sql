CREATE OR REPLACE FUNCTION update_refresh_token(_user_id BIGINT, _token UUID) RETURNS JSON
    LANGUAGE plpgsql AS
$$
DECLARE
    _token_id  BIGINT;
    _new_token UUID;
    _dt        TIMESTAMPTZ;
BEGIN
    _dt = NOW() AT TIME ZONE 'Europe/Moscow';

    WITH tokens AS (
        SELECT r.id,
               r.refresh_token,
               r.user_id
        FROM users u
            JOIN refresh_tokens r
            ON u.id = r.user_id
        WHERE u.id = _user_id)

    SELECT t.id
    INTO _token_id
    FROM tokens t
    WHERE t.refresh_token = _token;

    IF _token_id IS NULL THEN
        RAISE EXCEPTION 'refresh token is invalid';
    END IF;

    DELETE
    FROM refresh_tokens r
    WHERE r.id = _token_id;

    INSERT INTO refresh_tokens(refresh_token,
                               user_id,
                               dt)
    VALUES (gen_random_uuid(),
            _user_id,
            _dt)
    RETURNING refresh_token
    INTO _new_token;

    RETURN json_build_object('id', _user_id, 'refresh_token', _new_token);
END;
$$;
