CREATE TABLE IF NOT EXISTS users (
    id            BIGSERIAL
        PRIMARY KEY,
    email         VARCHAR(50)  NOT NULL UNIQUE,
    username      VARCHAR(50)  NOT NULL UNIQUE,
    password_hash VARCHAR(500) NOT NULL
);

CREATE TABLE IF NOT EXISTS refresh_tokens (
    id            BIGSERIAL
        PRIMARY KEY           NOT NULL,
    refresh_token UUID        NOT NULL,
    user_id       BIGINT      NOT NULL,
    dt            TIMESTAMPTZ NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (id)
        ON DELETE CASCADE
);

-- Готовые конфигурации в сооответствие с ГОСТами
CREATE TABLE IF NOT EXISTS document_config (
  id        BIGSERIAL
      PRIMARY KEY       NOT NULL,
  title     VARCHAR(50) NOT NULL,
  subtitle  VARCHAR(50) NOT NULL,
  main_text VARCHAR(50) NOT NULL
);

-- Таблица содержимого документа.
-- У содержимого есть тип (заголовок, текст, список, ...)
-- и тело, а также ссылка на следующий контент.
-- Одно тело контента не может превышать 500 символов
-- в целях оптимизации
CREATE TABLE IF NOT EXISTS document_content (
    id           BIGSERIAL
        PRIMARY KEY            NOT NULL,
    type         VARCHAR(10)   NOT NULL,
    body         VARCHAR(500)  DEFAULT '',
    next_content BIGINT        DEFAULT NULL,

    -- велика вероятность, что doc_id сильно
    -- облегчит нам жизнь в будущем.
    -- FK не добавлен для избежания циклической зависимости.
    doc_id       BIGINT        DEFAULT NULL
);

-- Очевидно таблица документов
CREATE TABLE IF NOT EXISTS documents (
    id                BIGSERIAL
        PRIMARY KEY          NOT NULL,
    title             VARCHAR(100)  NOT NULL,
    status            CHAR(15)      NOT NULL,
    created_at        TIMESTAMP     NOT NULL,
    updated_at        TIMESTAMP     NOT NULL,
    config_id         BIGINT        NOT NULL,
    content_start     BIGINT        DEFAULT NULL,
    doc_ref           VARCHAR(60)   NOT NULL,
    FOREIGN KEY (content_start) REFERENCES document_content (id)
        ON DELETE SET DEFAULT,
    FOREIGN KEY (config_id) REFERENCES document_config (id)
);

-- Таблица ролей и ее заполнение
CREATE TABLE IF NOT EXISTS roles (
    id      SERIAL
        PRIMARY KEY NOT NULL,
    name    VARCHAR(15)      NOT NULL UNIQUE,
    description VARCHAR(150) NOT NULL
);
INSERT INTO roles (name, description)
VALUES ('OWNER', 'Владелец документа'),
       ('COMMENTATOR', 'Пользователь с правами на комментирование'),
       ('EDITOR', 'Пользователь с правами на редактирование'),
       ('VIEWER', 'Пользователь с правами на просмотр')
ON CONFLICT (name) DO UPDATE
SET description=excluded.description;

-- Таблица связи документ+пользователь+роль
CREATE TABLE IF NOT EXISTS access_groups (
    doc_id      BIGSERIAL NOT NULL,
    user_id     BIGINT    NOT NULL,
    role_id     INT       NOT NULL,
    FOREIGN KEY (doc_id) REFERENCES documents (id)
        ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users (id)
        ON DELETE CASCADE,
    FOREIGN KEY (role_id) REFERENCES roles
        ON DELETE CASCADE
);

-- Справочник статусов.
-- Каждый статус имеет роль, применяемую новому
-- пользователю документа.
CREATE TABLE IF NOT EXISTS document_status (
    id                  SERIAL
        PRIMARY KEY NOT NULL,
    name                VARCHAR(15)   NOT NULL UNIQUE,
    description         VARCHAR(150)  NOT NULL,
    default_user_role   VARCHAR(15),
    FOREIGN KEY (default_user_role) REFERENCES roles (name)
        ON DELETE SET NULL
);
INSERT INTO document_status (name, default_user_role, description)
VALUES ('PRIVATE', NULL, 'Приватный доступ к документу. Никто не сможет получить доступ к документу, пока владелец сам не предоставит доступ'),
       ('PUB_READ', 'VIEWER','Любой пользователь имеющий ссылку на документ сможет читать его'),
       ('PUB_COMMENT', 'COMMENTATOR', 'Любой пользователь имеющий ссылку на документ сможет комментировать его'),
       ('PUB_WRITE', 'EDITOR', 'Любой пользователь имеющий ссылку на документ сможет редактировать его')
ON CONFLICT (name) DO NOTHING;